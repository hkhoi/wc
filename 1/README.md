## Warm the fuck back up!
Holidays are over and my brain is getting rusty. Time to warm that shit back up!

## Challenges
* daily-temperatures
* group-the-people-given-the-group-size-they-belong-to
* merge-k-sorted-lists
* next-greater-node-in-linked-list
* subarray-sum-equals-k
* number-of-submatrices-that-sum-to-target
* sort-array-by-parity-ii
* squares-of-a-sorted-array
* generate-parentheses
* greatest-common-divisor-of-strings
* palindromic-substrings
* parsing-a-boolean-expression
* score-of-parentheses
* intersection-of-two-arrays-ii
* longest-palindrome
* maximum-length-of-repeated-subarray
* smallest-range-covering-elements-from-k-lists
* subarray-sums-divisible-by-k
* top-k-frequent-words
* binary-search-tree-iterator
* design-hashmap
* design-hashset
* serialize-and-deserialize-binary-tree
