#include <bits/stdc++.h>
using namespace std;

class Solution {
private:
	void collapse(
		int index,
		stack<int>& indexStack,
		const vector<int>& nums,
		vector<int>& result
	){
		while (
			!indexStack.empty() &&
			nums[index] > nums[indexStack.top()] 
		) {
			result[indexStack.top()] = index - indexStack.top();
			indexStack.pop();
		}	
	}

public:	
	vector<int> dailyTemperatures(vector<int>& nums)
	{
		vector<int> result(nums.size(), 0);
		stack<int> indexStack;

		for (int i = 0; i < nums.size(); ++i) {
			collapse(i, indexStack, nums, result);
			indexStack.push(i);
		}

		return result;
	}
};

int main()
{   
}
